import re
import time
from datetime import datetime

import requests


class Repo:
    release_data: dict
    release_key: int | None

    def __init__(self):
        url = f"https://portswigger.net/burp/releases/data?previousLastId=-1&lastId=-1&t={int(time.time())}&pageSize=10"
        r = requests.get(url)
        self.release_data = r.json()
        self.release_key = None

    def version(self) -> str | None:
        if self.release_key is None:
            self.latest_timestamp()

        return self.release_data["ResultSet"]["Results"][self.release_key].get(
            "version"
        )

    def shasums(self) -> list[str] | None:
        if self.release_key is None:
            self.latest_timestamp()

        for build in self.release_data["ResultSet"]["Results"][self.release_key][
            "builds"
        ]:
            if build["ProductId"] == "pro" and build["ProductPlatform"] == "Jar":
                return [build.get("Sha256Checksum")]

    def latest_timestamp(self) -> datetime | None:
        date_pattern = r"(\d{1,2}\s\w+\s\d{4})"
        latest = None

        for i, release in enumerate(self.release_data["ResultSet"]["Results"]):
            if "Early Adopter" not in release.get("releaseChannels"):
                continue

            match = re.search(date_pattern, release.get("releaseDate"))
            if not match:
                continue

            date_string = match.group(1)
            timestamp = datetime.strptime(date_string, "%d %B %Y")

            if latest == None or timestamp > latest:
                latest = timestamp
                self.release_key = i

        return latest
