import subprocess


def run(*commands: str) -> int:
    result_code = 0
    for command in commands:
        process = subprocess.run(command, shell=True)
        result_code |= process.returncode
    return result_code


def format() -> int:
    return run("isort .", "black .")


def lint() -> int:
    return run("pyright .", "pyflakes .")


def test() -> int:
    return run("pytest .")
