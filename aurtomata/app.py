#!/usr/bin/env python

from cleo.application import Application

from aurtomata.cli import BuildCommand, RenderCommand

app = Application("aurtomata")
app.add(RenderCommand())
app.add(BuildCommand())


def run():
    app.run()


if __name__ == "__main__":
    run()
