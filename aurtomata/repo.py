from pathlib import Path

from aurtomata.db import Database
from aurtomata.template import RepoTemplate


class Repo:
    template: RepoTemplate
    db: Database
    name: str
    outdir: Path

    def __init__(self, name: str, indir: Path, outdir: Path):
        self.template = RepoTemplate(indir / name)
        self.db = Database()
        self.name = name
        self.outdir = outdir

    def has_newer(self) -> bool:
        if not self.db.is_name_in_database(self.name):
            return True
        latest_known = self.db.get_date(self.name)
        latest = self.template.repoclass.latest_timestamp()
        return latest > latest_known

    def version(self) -> str:
        return self.db.get_version(self.name)

    def render_update(self) -> str:
        latest = self.template.repoclass.latest_timestamp()
        version = self.template.repoclass.version()
        self.template.render(self.outdir / self.name)

        if not self.db.is_name_in_database(self.name):
            self.db.insert(self.name, version, latest, "")
            return version

        self.db.set_date(self.name, latest)
        self.db.set_version(self.name, version)
        return version
