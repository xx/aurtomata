import shutil
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp

from git import Repo as repo_plumbing


class GitRepo:
    env: dict
    url: str
    repo: repo_plumbing
    workdir: Path

    def __init__(self, name: str, keypath: str, url=None):
        self.env = {"GIT_SSH_COMMAND": f"ssh -i {keypath}"}

        if url is not None:
            self.url = url
        else:
            self.url = f"ssh://aur@aur.archlinux.org/{name}.git"

        self.workdir = Path(mkdtemp(prefix=f"aurtomata-{name}-"))
        self.repo = repo_plumbing.clone_from(self.url, self.workdir, env=self.env)

    def __del__(self):
        rmtree(self.workdir)

    def merge_changes(self, rendered_dir: str, version: str) -> bool:
        # delete all items
        # we do this to git rm files deleted from the template
        for item in self.workdir.iterdir():
            if item.is_dir() and item.name == ".git":
                continue
            elif item.is_dir():
                for sub in item.rglob("*"):
                    sub.unlink() if sub.is_file() else sub.rmdir()
                item.rmdir()
            else:
                item.unlink()

        # add files from rendered template
        rendered_path = Path(rendered_dir)
        for item in rendered_path.rglob("*"):
            if item.is_file():
                target = self.workdir / item.relative_to(rendered_path)
                target.parent.mkdir(parents=True, exist_ok=True)
                shutil.copy2(item, target)

        self.repo.git.add(A=True)

        if self.repo.is_dirty(untracked_files=True):
            self.repo.index.commit(f"update to {version}")
            return True

        return False
