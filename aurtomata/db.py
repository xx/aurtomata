import sqlite3
from datetime import datetime

time_fmt = "%Y-%m-%d %H:%M:%S"


class Database:
    c: sqlite3.Connection

    def __init__(self, url="/tmp/testdb.sqlite3"):
        self.c = sqlite3.connect(url)
        self.c.execute("CREATE TABLE IF NOT EXISTS latest(name, version, date, data);")
        self.c.execute("CREATE UNIQUE INDEX IF NOT EXISTS idx_name ON latest(name);")

    def __del__(self):
        self.c.close()

    def is_name_in_database(self, name: str) -> bool:
        c = self.c.execute(
            "SELECT EXISTS(SELECT 1 FROM latest WHERE name = ?)", (name,)
        )
        return c.fetchone()[0] == 1

    def insert(self, name: str, version: str, date: datetime, data: str):
        self.c.execute(
            "INSERT INTO latest (name, version, date, data) VALUES (?, ?, ?, ?)",
            (name, version, date, data),
        )
        self.c.commit()

    def get_version(self, name: str) -> str:
        c = self.c.execute("SELECT version FROM latest WHERE name = ?", (name,))
        return c.fetchone()[0]

    def get_date(self, name: str) -> datetime:
        c = self.c.execute("SELECT date FROM latest WHERE name = ?", (name,))
        return datetime.strptime(c.fetchone()[0], time_fmt)

    def get_init_data(self, name: str) -> str:
        c = self.c.execute("SELECT version FROM latest WHERE name = ?", (name,))
        return c.fetchone()[0]

    def set_version(self, name: str, version: str):
        self.c.execute("UPDATE latest SET version = ? WHERE name = ?", (version, name))
        self.c.commit()

    def set_date(self, name: str, date: datetime):
        datestr = date.strftime(time_fmt)
        self.c.execute("UPDATE latest SET date = ? WHERE name = ?", (datestr, name))
        self.c.commit()

    def set_data(self, name: str, data: str):
        self.c.execute("UPDATE latest SET data = ? WHERE name = ?", (data, name))
        self.c.commit()
