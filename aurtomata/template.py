import importlib.util
import shutil
from pathlib import Path
from typing import Any

from jinja2 import Template


class NoPkgbuildTemplateException(Exception):
    pass


class NoRepoPyException(Exception):
    pass


class RepoTemplate:
    repoclass: Any
    template: Path

    def __init__(self, repodir: Path):
        self.template = repodir

        repo_py = self.template / "repo.py"
        if not repo_py.exists():
            raise NoRepoPyException

        spec = importlib.util.spec_from_file_location("repo", repo_py)
        if spec is None or spec.loader is None:
            raise NoRepoPyException
        repo = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(repo)

        self.repoclass = repo.Repo()

    def render(self, out: Path):
        pkgbuild = self.template / "PKGBUILD.jinja"
        if not pkgbuild.exists():
            raise NoPkgbuildTemplateException

        out.mkdir(parents=True)

        for file in self.template.glob("*"):
            if file.name in ["repo.py", "PKGBUILD.jinja"]:
                continue
            if file.is_dir():
                continue
            shutil.copy(file, out / file.name)

        with open(pkgbuild, "r") as f:
            t = Template(f.read())

        with open(out / "PKGBUILD", "w") as f:
            f.write(
                t.render(version=self.repoclass.version, shasums=self.repoclass.shasums)
            )
