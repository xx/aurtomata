from pathlib import Path
from shutil import rmtree

from cleo.commands.command import Command
from cleo.helpers import argument, option

from aurtomata.ci import Supervisor
from aurtomata.repo import Repo
from aurtomata.template import NoPkgbuildTemplateException, NoRepoPyException

basedir = Path().home() / ".local/aurtomata"


class RenderCommand(Command):
    name = "render"
    description = "Render an AUR repository from a template"
    arguments = [
        argument("template", description="Name of template to render"),
        argument(
            "output",
            description="Where to store the rendered repository",
            default=str(basedir / "out"),
            optional=True,
        ),
    ]
    options = [
        option(
            "templates-dir",
            "t",
            description="Location of directory where templates are stored",
            default=str(basedir / "templates"),
            flag=False,
        ),
        option(
            "no-version-check",
            "N",
            description="Skip checking version, always rendering the template",
        ),
        option(
            "overwrite",
            "o",
            description="Overwrite output directory if it already exists",
        ),
    ]

    def handle(self) -> int:
        try:
            repo = Repo(
                self.argument("template"),
                Path(self.option("templates-dir")),
                Path(self.argument("output")),
            )
        except NoRepoPyException:
            template_dir = Path(self.option("templates-dir")) / self.argument(
                "template"
            )
            self.line(
                f"<error>Can not find <info>repo.py</info> in <info>{template_dir}</info></error>"
            )
            return 1

        self.line(f"Parsing template <info>{repo.name}</info>")

        if not self.option("no-version-check") and not repo.has_newer():
            self.line(
                f"Template already at newest version <info>{repo.version()}</info>, doing nothing"
            )
            self.line("Hint: Pass <comment>-N</comment> to override")
            return 0

        output_dir = Path(self.argument("output")) / self.argument("template")
        if self.option("overwrite") and output_dir.exists():
            rmtree(output_dir)

        try:
            version = repo.render_update()
        except NoPkgbuildTemplateException:
            template_dir = Path(self.option("templates-dir")) / self.argument(
                "template"
            )
            self.line(
                f"Can not find <info>PKGBUILD.jinja</info> in <info>{template_dir}</info>",
                "error",
            )
            return 1

        self.line(
            f"Rendered version <info>{version}</info> to <info>{repo.outdir / repo.name}</info>"
        )
        return 0


class BuildCommand(Command):
    name = "build"
    description = "Try building a single package using 'makepkg'"
    arguments = [
        argument(
            "repo", description="Location of directory where the PKGBUILD is located"
        )
    ]
    options = [
        option(
            "no-update-srcinfo",
            "S",
            description="Do not write a new .SRCINFO file",
        ),
    ]

    def handle(self) -> int:
        fpath = Path(self.argument("repo")).resolve()

        if not fpath.exists():
            self.line(f"No such file or directory: <info>{fpath}</info>", "error")
            return 1
        elif not fpath.is_dir():
            self.line(f"Path <info>{fpath}</info> is not a directory!", "error")
            return 1
        elif not (fpath / "PKGBUILD").exists():
            self.line(f"No PKGBUILD found in <info>{fpath}</info>", "error")
            return 1

        basename = fpath.stem
        s = Supervisor()

        self.line(f"Building fresh Arch Linux image: <info>{s.image_name}</info>")
        s.ensure_up_to_date()

        self.line(
            f"Building <info>{basename}</info> in container <info>{s.name_container(basename)}</info>"
        )

        success = s.build(basename, fpath, wait=True)

        if success:
            self.line("Build successful")
        else:
            self.line("Build failed! Container logs:", "error")
            print(s.logs(basename))
            return 2

        if not self.option("no-update-srcinfo"):
            self.line(
                f"Updating .SRCINFO for <info>{basename}</info>"
            )
            s.update_srcinfo(basename, fpath)

        return 0
