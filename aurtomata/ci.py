from io import BytesIO
from pathlib import Path
from random import choices
from string import ascii_lowercase, digits

import docker
from docker.models.containers import Container

dockerfile = b"""
FROM archlinux:base-devel

ENV PKGDEST=/tmp
ENV SRCDEST=/tmp
ENV SRCPKGDEST=/tmp
ENV LOGDEST=/tmp
ENV BUILDDIR=/tmp

RUN useradd -m aurtomata && \\
      echo 'aurtomata ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/aurtomata
RUN pacman -Syu --noconfirm

USER aurtomata
"""
repo_mount_point = "/home/aurtomata/repo"


class SupervisorException(Exception):
    pass


class Supervisor:
    dc: docker.DockerClient
    containers: dict[str, Container]
    done_update: bool
    image_name: str

    def __init__(self):
        self.containers = {}
        self.done_update = False
        self.image_name = "aurtomata-" + "".join(choices(ascii_lowercase + digits, k=6))
        self.dc = docker.from_env()

    def __del__(self):
        for container in self.containers.values():
            container.stop()
            container.remove()

        try:
            self.dc.images.remove(image=self.image_name, force=True)
        except AttributeError:
            pass

    def ensure_up_to_date(self) -> bool:
        # ensures that for this session, the arch base image has been updated.
        # creates a new temporary image "aurtomata-xxxxxx"
        if self.done_update:
            return True

        dockerfile_obj = BytesIO(dockerfile)
        self.dc.images.build(
            fileobj=dockerfile_obj, pull=True, tag=self.image_name, forcerm=True
        )
        dockerfile_obj.close()

        return False

    def run(self, name: str, cmd: str, dir: Path = Path("")) -> Container:
        self.ensure_up_to_date()
        dir_path = dir.resolve()
        volumes = {}
        if dir != Path(""):
            volumes[str(dir_path)] = {"bind": repo_mount_point, "mode": "ro"}

        container = self.dc.containers.run(
            self.image_name,
            cmd,
            detach=True,
            working_dir=repo_mount_point,
            volumes=volumes,
            name=self.name_container(name),
        )

        self.containers[name] = container
        return container

    def name_container(self, name: str) -> str:
        return f"{self.image_name}-{name}"

    def build(self, name: str, dir: Path, wait=False) -> bool:
        # attempts to build PKGBUILD in specified "dir" using specified container "name"
        cmd = "makepkg -sfc --noconfirm --noprogressbar"
        self.run(name, cmd, dir)

        if wait:
            return self.wait_success(name)
        return True

    def update_srcinfo(self, name: str, dir: Path) -> bool:
        # runs "makepkg --printsrcinfo" in the container
        # and saves the output to `$dir/.SRCINFO`
        # todo: we should probably wait until the build container is done?
        self.ensure_up_to_date()
        dir_path = dir.resolve()
        volumes = {}
        if dir != Path(""):
            volumes[str(dir_path)] = {"bind": repo_mount_point, "mode": "rw"}

        output = self.dc.containers.run(
            self.image_name,
            "makepkg --printsrcinfo",
            working_dir=repo_mount_point,
            volumes=volumes,
            stdout=True,
            stderr=True,
            name=f"{self.name_container(name)}-srcinfo"
        )

        output_str = output.decode("utf-8")

        with open(dir / ".SRCINFO", "w") as f:
            f.write(output_str)

        return True

    def finished(self, name: str) -> bool:
        # returns whether the container is finished
        container = self.containers.get(name)
        if container is None:
            raise SupervisorException(f"container '{name}' missing")
        return container.status != "running"

    def wait_success(self, name: str) -> bool:
        # waits for the container to exit and prints whether it ran successfully
        container = self.containers.get(name)
        if container is None:
            raise SupervisorException(f"container '{name}' missing")
        return container.wait().get("StatusCode") == 0

    def logs(self, name: str) -> str | None:
        # returns logs for container by name
        container = self.containers.get(name)
        if container is None:
            raise SupervisorException(f"container '{name}' missing")

        container.wait()
        return container.logs(timestamps=True).decode("utf-8")
